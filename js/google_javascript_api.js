(function($) {
  function init() {
    gapi.client.setApiKey('YOUR API KEY');
    gapi.client.load('urlshortener', 'v1').then(makeRequest);
  }
})(jQuery);